<%--
  Created by IntelliJ IDEA.
  User: Tsyklop-Laptop
  Date: 06.08.2022
  Time: 12:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Login</title>
    </head>
    <body>
        <form action="/helloapp/login"
              method="POST">

            <%if("true".equals(request.getAttribute("error"))) {%>
                <h5 style="color: red"><%=request.getAttribute("errorMsg")%></h5>
            <%}%>

            <input type="text" name="login"/>
            <input type="password" name="password"/>

            <%-- 2 Вводим логин и проль и нажимаем на кнопку входа --%>

            <button type="submit">
                Login
            </button>

        </form>
    </body>
</html>

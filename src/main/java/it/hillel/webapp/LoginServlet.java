package it.hillel.webapp;

import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    private final LoginService loginService = new LoginService();

    @Override
    public void init() throws ServletException {
        super.init();
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();

        // 1. Запрашиваем страницу в браузере: переходим по адресу http://localhost:8080/helloapp/login
        request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // 3. Обрабатываем запрос со страницы login тут. Достаем параметры формы и делаем проверку введенных данных.

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {

            // 4. Проверяем логин и пароль

            this.loginService.login(login, password);

            request.getSession().setAttribute("user", "true");

            // 4.1. Если ошибки нет, значит вход удачен - перенаправляем на страницу dashboard
            response.sendRedirect(getServletContext().getContextPath() + "/dashboard");

        } catch (ApplicationException e) {
            // 4.2. Если возникла ошибка возвращаем пользователя на страницу login и показываем ошибку
            request.setAttribute("error", "true");
            request.setAttribute("errorMsg", e.getMessage());
            request.getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
        }

    }

}

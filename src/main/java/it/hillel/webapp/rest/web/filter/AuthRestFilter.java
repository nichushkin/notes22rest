package it.hillel.webapp.rest.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.rest.component.SecurityContext;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.service.UserService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@WebFilter(filterName = "AuthRestFilter", urlPatterns = "/*")
public class AuthRestFilter implements Filter {

    private JwtParser jwtParser;

    private final UserService userService = new UserService();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final List<String> PROTECTED_PATHS = new ArrayList<>();


    // Вызывается один раз, при создании объекта фильтра веб-сервером
    public void init(FilterConfig config) throws ServletException {

        this.jwtParser = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)))
                .build();

        // Список путей, которые защищены.
        PROTECTED_PATHS.add("/api/v1/user");

    }

    //
    public void destroy() {

    }

    // Вызывается на каждый запрос
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        // Убираем из пути запроса контекст веб приложения (по сути то же имя приложения для веб-сервера).
        // /helloapp - имя приложения или путь, по которому доступно это самое веб приложение
        String requestUri = req.getRequestURI().replace(req.getContextPath(), "");

        // Проверка наличия пути запроса в коллекции защищенных путей.
        // Если путь найден в коллекции, то мы должны проверить авторизован ли пользователь.
        if (PROTECTED_PATHS.stream().anyMatch(url -> url.startsWith(requestUri))) {

            // Достаём закголовок с токеном авториации
            String authHeader = req.getHeader("Authorization");

            // если его нет, возвращаем ошибку
            if (authHeader == null || authHeader.isEmpty()) {

                resp.setStatus(401);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto("Где токен?")));
                resp.getWriter().flush();

                return;

            }

            // убираем приставку Bearer
            String jwtToken = authHeader.replace("Bearer ", "");

            // При помощи библиоткеи парсим токен и проверяем его сроки действия и т.д.
            Claims claims = this.jwtParser.parseClaimsJws(jwtToken).getBody();

            // Достаем значение ключа subject и ищем по этому значению пользователя в БД (В реальных приложениях).
            // Если пользователь не найден - бросаем ошибку клиенту.
            // Если найден, то значит пользователь авторизован. Пароль тут не проверяем ибо нет оригинала.
            // НЕ НУЖЖНО ДОБАВЛЯТЬ ПАРОЛЬ В JWT ТОКЕН!!!
            if (!claims.getSubject().equals("admin")) {

                resp.setStatus(401);
                resp.setContentType("application/json");
                resp.setCharacterEncoding("UTF-8");
                resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto("Токен не такой")));
                resp.getWriter().flush();

                return;

            }

            // ПРи успешной проверки токена и пользователя добавляем пользователя в хранилище для потока текущего.
            // В дальнейшем текущий пользователь может понадобиться для загрузки данных
            SecurityContext.set(this.userService.findByLogin("admin"));

            // Вызываем цепочку фильтров далее. Если это не сделать - запрос дальше не пойдет и ответа не будет.
            chain.doFilter(request, response);

        } else {
            chain.doFilter(request, response);
        }

    }

}

package it.hillel.webapp.rest.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.exception.ApplicationException;
import it.hillel.webapp.rest.model.TokenDto;
import it.hillel.webapp.rest.dto.LoginDto;
import it.hillel.webapp.rest.dto.ResponseDto;
import it.hillel.webapp.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.UUID;

@WebServlet(urlPatterns = "/api/v1/auth/login", loadOnStartup = 1)
public class LoginRestServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final LoginService loginService = new LoginService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // Считываем тело запроса, которое в json формате, в DTO объект для дальнейшего использования данных запроса
        LoginDto dto = this.objectMapper.readValue(req.getInputStream(), LoginDto.class);

        // Проверка данных от клиента если нужно

        try {

            Date now = new Date();

            // Вызываем бизнес логику
            UserEntity user = this.loginService.login(dto.getLogin(), dto.getPassword());

            // Создаем jwt токен для авторизации.
            // Этот токен нужно будет добавлять в заголовок запроса Authorization с приставкой Bearer.
            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setExpiration(new Date(now.getTime() + 1800000))
                    .setIssuedAt(now)
                    .setId(UUID.randomUUID().toString())
                    .signWith(Keys.hmacShaKeyFor("Yn2kjibddFAWtnPJ2AFlL8WXmohJMCvigQggaEypa5E=".getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
                    .compact();

            // Пишем ответ клиенту.
            resp.setStatus(200);
            resp.setContentType("application/json");
            resp.getWriter().write(this.objectMapper.writeValueAsString(new TokenDto(token))); // преобразовуем java объект в json строку.
            resp.getWriter().flush();

        } catch (ApplicationException e) {
            resp.setStatus(400);
            resp.setContentType("application/json");
            resp.getWriter().write(this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
            resp.getWriter().flush();
        }

    }

}



package it.hillel.webapp.rest.note;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

//@WebServlet(name = "UserRestServlet", urlPatterns = "/api/v1/notes")
public class NoteServlet extends HttpServlet {

    private final NoteService service = new NoteService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String out;
        Integer id = Integer.getInteger(request.getParameter("id"));
        if (id != null) {
            out = new ObjectMapper().writeValueAsString(service.getNote(id));
        } else {
            List<Note> notes = service.getNotes();
            out = new ObjectMapper().writeValueAsString(notes);
        }
        response.getWriter().write(out);
        request.getRequestDispatcher("/note.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Note out;
        Integer id = Integer.getInteger(request.getParameter("id"));
        if (id != null) {
            out = service.edit(id, request.getParameter("title"), request.getParameter("desc"));
        } else
            out = service.create(request.getParameter("title"), request.getParameter("desc"));
        String json = new ObjectMapper().writeValueAsString(out);
        response.getWriter().write(json);
    }


    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.getInteger(req.getParameter("id"));
        if (id != null) {
            service.deleteNote(id);
        } else throw new IllegalArgumentException("id not found");
    }
}
package it.hillel.webapp.rest.note;

import java.util.*;

public class NoteService {

    Map<Integer, Note> storage = new HashMap<>() {{
        put(1, new Note(1, "sometitel1", "somedescription1"));
        put(2, new Note(2, "sometitel2", "somedescription2"));
        put(3, new Note(3, "sometitel3", "somedescription3"));
    }};

    public List<Note> getNotes() {
        return new ArrayList<>(storage.values());
    }

    public Note getNote(int id) {
        return storage.get(id);
    }

    public void deleteNote(int id) {
        storage.remove(id);
    }

    public Note create(String title, String desc) {
        int id = 0;
        while (true) {
            if (storage.containsKey(id)) {
                id++;
            } else {
                break;
            }
        }
        Note n = new Note(id, title, desc);
        storage.put(id, n);
        return n;
    }

    public Note edit(int id, String title, String desc) {
        Note note;
        if (storage.containsKey(id)) {
            note = storage.get(id);
            note.setTitle(title);
            note.setDescription(desc);
        } else {
            throw new IllegalArgumentException("id not found");
        }
        return note;
    }
}
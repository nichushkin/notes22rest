package it.hillel.webapp.service;

import it.hillel.webapp.rest.entity.UserEntity;
import it.hillel.webapp.exception.ApplicationException;

public class LoginService {

    public UserEntity login(String login, String password) {

        // В реальном прилодении:
        //      Загружаем пользователя из БД. Если не найден - бросаем exception
        //      Проверяем пароль: пароль от клиента шифруем и сравниваем с тем, что в БД. Если не совпадают - бросаем exception
        //      Запросы в БД должны осуществляться через DAO объект.

        // В данном случае иметируем запросы в БД :)
        if (!login.equals("admin") || !password.equals("qwerty")) {
            throw new ApplicationException("Login or password incorrect");
        }

        return new UserEntity(login, "Test", "Test", password);

    }

    public void register(String firstName, String lastName, String login, String password) {

    }

}

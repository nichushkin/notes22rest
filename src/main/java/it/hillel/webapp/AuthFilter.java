package it.hillel.webapp;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthFilter implements Filter {

    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        HttpSession session = req.getSession();

        if (req.getRequestURI().endsWith("/dashboard") && session.getAttribute("user") != null) {
            chain.doFilter(request, response);
        } else {

            if (req.getRequestURI().endsWith("/login") || req.getRequestURI().endsWith("/register")) {
                chain.doFilter(request, response);
                return;
            }

            resp.sendRedirect(req.getContextPath() + "/login");

        }

    }

}
